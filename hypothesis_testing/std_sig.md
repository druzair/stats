# Practical Significance of Standard Deviation

## Tchebysheff's Rules
- _At least_ $`[1 - \frac{1}{k^2}]`$ of the measurements will lie within _k_ standard deviations
- It gives a _lower bound_ to the fraction of measurements to be found in $`\bar{x} \pm ks `$ interval

## Empirical Rule
Given the measurements are normally distributed
$`\mu \pm k\sigma`$ will contain approximately $`68\%, 96\%, 99.7\%`$ of the measurements for $`k = 1, 2, 3`$

### Example
With $`\bar{x}=12`$ and $`s=2.3`$ breaths/minute, what fraction of people will have breathing rate in the range of 
- 9.7 to 14.3 bpm
- 7.4 to 16.6 bpm
- More than 18.0 of less than 5.1 bpm
