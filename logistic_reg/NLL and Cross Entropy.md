# Negative log likelihood (NLL) and cross-entropy 

## 1. Negative Log Likelihood (NLL):
Suppose we have a probabilistic model with parameters θ, and we want to estimate the optimal values for θ given some observed data x. The likelihood function L(θ) represents the probability of observing the data x given the model parameters. The negative log likelihood is then calculated as:

$NLL(θ) = -log L(θ)$

In practice, we often work with the log-likelihood rather than the likelihood directly because it simplifies calculations and avoids numerical underflow issues.

## 2. Cross-Entropy:
In the context of classification tasks, suppose we have a predicted probability distribution $y_{pred}$ over C classes and a true probability distribution $y_{true}$ (often one-hot encoded) representing the ground truth. The cross-entropy loss between the predicted and true distributions is given by:

$Cross-Entropy = - ∑_{i=1}^C y_{true}^{(i)} * log (y_{pred}^{(i)})$

In this formula, $y_{true}^{(i)}$ represents the true probability of class i, and $y_{pred}^{(i)}$ represents the predicted probability of class i according to the model. The summation is taken over all C classes. Note that the logarithm used is typically the natural logarithm (base e).

By minimizing the cross-entropy loss, we aim to make the predicted class probabilities (y_pred) as close as possible to the true class probabilities (y_true), thereby improving the model's classification performance.

It's important to note Negative log-likelihood (NLL) and cross-entropy are mathematically equivalent when applied to classification tasks, and both can serve as loss functions. 

Cross-Entropy Loss is commonly used with models that output raw logits (unnormalized scores). It internally applies softmax before computing NLL.

NLL Loss assumes that probabilities (log probabilities) are already provided, meaning the model must apply log-softmax before passing the output to NLL.

| Data  | Class | Class A Probability $y_{pred}$ | Class B Probability $y_{pred}$ | Class A True Probability $y_{true}$ | Class B True Probability $y_{true}$ | Cross-Entropy |
|-------|-------|-----------------------------|-----------------------------|----------------------------------|----------------------------------|---------------|
| 1 | A     | 0.9                         | 0.1                         | 1.0                              | 0.0                              | 0.105         |
| 2 | A     | 0.7                         | 0.3                         | 1.0                              | 0.0                              | 0.356         |
| 3 | A     | 0.8                         | 0.2                         | 1.0                              | 0.0                              | 0.223         |
| 4 | B     | 0.3                         | 0.7                         | 0.0                              | 1.0                              | 0.357         |
| 5 | B     | 0.2                         | 0.8                         | 0.0                              | 1.0                              | 0.223         |
| 6 | B     | 0.4                         | 0.6                         | 0.0                              | 1.0                              | 0.510         |

In the Cross-Entropy column, you can see the calculated cross-entropy loss for each datapoint using the formula mentioned earlier. Each value represents the dissimilarity between the predicted probabilities ($y_{pred}$) and the true probabilities ($y_{true}$) for that particular datapoint. The cross-entropy values provide an indication of how well the model's predicted probabilities align with the true probabilities.

Note that the cross-entropy values may vary depending on the specific predicted and true probabilities for each datapoint. In this example, we calculated the cross-entropy loss using the negative logarithm of the predicted probabilities for the true class.
