# Concepts <a name="toc"></a>

1. [Introduction](#intro) 
2. [The Sigmoid Function](#LINK)
3. [Important Concepts](#Transformations)
    - [Probability](#probability), [Odds](#odds), [Log of Odds](#logodds), [Odds Ratio](#oddratio)
4. [Example 1](#Example1)
5. [Connecting Probability and Odds](#connecting_dots)
    - [The Null Model](#no_pred)
    - [Categorical Predictor](#cat_pred)
    - [Numerical Predictor](#num_pred)
    - [Multiple Predictors](#mult_pred)
6. [Dummy Variables](#dum_vars)
7. [Reference Level](#ref_level)
8. [Visualizing Logistic Regression](#vis_glm)
9. [Effect of normalization](#normalization_effect)
10. [Interpret Model outputs](#model_output)

------------
# Introduction <a name="intro"></a>
When the outcome is binary, quantifying the effect of one unit change in independent variables on the dependent variable in terms of probability.

# The Sigmoid Function <a name="LINK"></a>
How the linear model is tranformed to probability.

```math
y = \frac{1}{1 + e^{-(w^{T}X)}}
```

# Important Transformations <a name="Transformations"></a>

## Probability <a name="probability"></a>
```math
p =   \frac{n}{N} = \frac{odds}{1+odds} =\frac{e^{w^{T}X}}{1+e^{w^{T}X}} =   \frac{1}{1+e^{-(w^{T}X)}}
```
range between 0~1  [0 (if w<sup>T</sup>X) a large -ve), 1 ((if w<sup>T</sup>X a large +ve))). 

## Odds <a name="odds"></a>
```math
\\  odds          =  \frac{p}{1-p}                 =   e^{(w^{T}X)} 
```
## Log of Odds <a name="logodds"></a>
```math
  logit(p)  = log(odds)  =   log(\frac{p}{1-p}) =  log(e^{(w^{T}X)})   = (w^{T}X)  
``` 
range between -inf ~ +inf


```math
e^{log(odds)}=   e^{log(\frac{p}{1-p}})            =   odds                  =   e^{(w^{T}X)}
```
## Odds Ratio <a name="oddsratio"></a>
ratio of odds for different outcomes e.g event 1 and event 2. The OR is a way to express the strength of association between inputs and outcomes. 
- If the OR is <1, odds are decreased for an outcome; 
- If the OR is >1, ordds are increased for an outcome; 
```math
oddsratio =   \frac{odds1}{odds2} 
```
The slope $`m`$ is **log of oddsratio**
```math
m         =   log(odds1) - log(odds2)     =   log(\frac{odds1}{odds2})      =   log(oddsratio)   
```
Exponentiating slope give **oddsratio**
```math
e^m       =   e^{log(oddsratio)}         =   oddsratio        
```

*Logistic regression is in reality an ordinary regression using the logit as the response variable.*
The logit transformation transforms a linear relationship between the response variable in terms of logodds. 
So, the slope m is rate of change in logoddds of dependent variable for one unit change in respective independent variable.

[TOC](#toc)

# Example <a name="Example1"></a>

```r
hon_data <- read.csv("https://raw.githubusercontent.com/DrUzair/MLSD/master/Regression/sample.csv")
library(gmodels)
CrossTable(hon_data$hon, hon_data$female, expected = T, format="SPSS")
CrossTable(hon_data$hon, hon_data$female, prop.t=F, prop.r=F, prop.c=F, prop.chisq=F, format="SPSS")
```
hon_data     | Male | Female | Row Total
------------ | --- | --- | -------------
No Hon       | 74  | 77  | 151
Hon          | 17  | 32  | 49   
Column Total | 91  | 109 | 200            

[TOC](#toc)
 
# Connecting the Probability and Odds <a name="connecting_dots"></a>

## The Null Model <a name="no_pred"></a>

No predictors. just the intercept 
```r
glm(hon~1, hon_data, family = 'binomial')

**Coefficients:**
(Intercept)  
 -1.125  
```
hon_data     | Male | Female | Row Total
------------ | --- | --- | -------------
No Hon       | 74  | 77  | 151
Hon          | 17  | 32  | 49   
Column Total | 91  | 109 | 200      

 * log(odds) = log(p/(1-p)) = -1.125
 * the logodds of any gender student in hon class: positive means more likely w.r.t [reference level](#ref_level)
 * p = overall prob of being in honors class = n/N = 49/200 = exp(-1.125)/(1+exp(-1.125))= 0.245

How about linear regression ?
```r
lm(hon~1, hon_data)

**Coefficients:**
(Intercept)  
      0.245 
```
So ? GLM is just $` log(\frac{p}{1-p})=log(\frac{0.245}{1-0.245})=-1.125 `$

```math
log(\frac{p}{1-p})=log(\frac{0.245}{1-0.245})=-1.125
```

[TOC](#toc)

## The case of one categorical predictor <a name="cat_pred"></a>
```r
glm(hon~female, hon_data, family = 'binomial')

 Coefficients: 
  (Intercept)       female  
   -1.4709       0.5928  
```
hon_data     | Male | Female | Row Total
------------ | --- | --- | -------------
No Hon       | 74  | 77  | 151
Hon          | 17  | 32  | 49   
Column Total | 91  | 109 | 200 

 * The Intercept represents **[log of odds](#logodds)** 
    * The logodds of a male in hon class
        * $` log(17/74) = -1.4709 `$ --> Intercept $` Intercept + female \times 0 = -1.4709 `$
        * the probability of a male in honors class $`p=\frac{17}{91}=\frac{1}{1+e^{-(w^{T}X)}}=\frac{1}{1+e^{-(0.598x0-1.4709)}}=0.1868`$
    * The logodds of a female in hon class
        * $` log(32/77) = log(0.4155) = - 0.87 `$ --> $` Intercept + female \times 1 = -1.4709 + 0.5928 = -0.87 `$
        * the probability of a female in honors class $`p=\frac{32}{109}=\frac{1}{1+e^{-(w^{T}X)}}=\frac{1}{1+e^{-(0.598x1-1.4709)}}=0.2935`$
 * The coefficient of binary input represents **log of oddsratio** of dependent variable for a non-zero value of the input variable
    * the [**oddsratio**](#oddsratio) between female & male = (32/77) / (17/74) = 1.809
    * oddsratio = exp(coef of female) --> exp(0.5928) --> 1.809 --> female 80% more likely to be in hon class

Want to try linear regression again ?
```R
lm(formula = hon ~ female, data = hon_data)

Coefficients:
(Intercept)       female  
     0.1868       0.1068  
```
Here, the intercept represents **probability** $` P(Hon | Female = 0) = 0.1868   +    0.1068 \times 0= 0.1868`$. 

Convert probability to logodds. GLM is doing the same; $` log(odds)=log(\frac{p}{1-p})=log(0.1868/(1-0.1868))=-1.4709 `$.

oh and yes, if the value for female is 1 ...

Then the probability of a randomly picked female student being honors student is $` P(Hon | Female = 1) = 0.1868   +    0.1068 \times 1=0.2936 `$.

Care to convert it to logodds ? 
$` log(odds)=log(\frac{p}{1-p})=log(0.2936/(1-0.2936))=-0.87 `$ 
Surprised ?

So, the slope of linear regression model (on binary classification problems) represents 
**change in probability** of dependent variable for a unit change in indepent variable.

[TOC](#toc)

## The case of one continuous predictor <a name="num_pred"></a>
```r
glm(hon~math, hon_data, family = 'binomial')

Coefficients:
 (Intercept)   math  
 -9.7939       0.1563  
```
* math coef
   * consider two consecutive numbers. say 40 and 41
   * placing 40 and 41 in the equation and taking the diff  $`(41*0.1563 - 9.7939) - (40*0.1563 - 9.7939)`$ will give 0.1563
   * Thus math coef is "the change in log of [oddsratio](#oddsratio) for two consecutive values in math score"
 * So. **one unit increment in math score** will result in **0.1563 increment in log of oddsratio**
 * Translate change in 'log of oddsratio' to change in 'odds ratio'
   * Change in $`log(oddsratio) = log(\frac{odds(math=41)} {odds(math=40))}`$
     - odds(math=41) = exp(41*0.1563-9.7939) = 0.03385732
     - odds(math=40) = exp(40*0.1563-9.7939) = 0.02895825
   * Change in log(oddsratio) = log(0.03385732/0.02895825)= 0.1563 
   * Change in $`oddsratio = e^{0.1563} = 1.169224`$ ~ 1 additonal number in math score 16% more likely to be in hon


***NOTE:*** In case the independent variable is continuous, there is no relationship between lm and glm. The logistic regression coefficients are calculated using Iterative WLS algorithm.

[TOC](#toc)

## The case of multiple predictors (categorical and numeric) <a name="mult_pred"></a>
```r
glm(hon~math+read+female, hon_data, family = 'binomial')

**Coefficients:**
 (Intercept)      math         read        female  
 -11.77025      0.12296      0.05906      0.97995  
```
 * math: keeping read and female constant, one-unit increase in math increases the [oddsratio](#oddsratio) of hon by exp(0.1229589)
 * read: keeping math and female constant, one-unit increase in read increases the [oddsratio](#oddsratio) of hon by exp(0.0590632)
 * female: keeping read and math constant, the log of oddsratio for females is 0.97995 for being in hon class.
### Interaction analysis
    * assess the extent to which the association between one predictor and the outcome depends on a second predictor.
```r
glm(hon~ female + math + female:math, hon_data, family = 'binomial')
# glm(hon~ female + math + female*math, hon_data, family = 'binomial') --> same thing

**Coefficients:**
  (Intercept)       female         math  female:math  
       8.7458       2.8999      -0.1294      -0.0670  
```
 * Adding an interaction term to a model drastically changes the interpretation of all of the coefficients. 
 * If there were no interaction term, -0.1294 would be interpreted as the unique effect of math on hon. 
 * But the interaction means that the effect of math on hon is different for different values of female.  
 * So the unique effect of math on hon is not limited to math coef(-0.1294), but also depends on the values of female and -0.0670. 

[TOC](#toc)

## Dummy Variables <a name="dum_vars"></a>
Regression model treates catategorical variables treats differently than continuous ones.

## Reference Level in R <a name="ref_level"></a>
```r
hon_data$hon <- as.factor(hon_data$hon)
str(hon_data$hon)
levels(hon_data$hon)
hon_data$hon <- relevel(hon_data$hon, '1')
levels(hon_data$hon)
CrossTable(hon_data$hon, hon_data$female, prop.t=F, prop.r=F, prop.c=F, prop.chisq=F, format="SPSS")

               | hon_data$female 
hon_data$hon   |        0  |        1  | Row Total | 
  -------------|-----------|-----------|-----------|
             1 |       74  |       77  |      151  | 
  -------------|-----------|-----------|-----------|
             0 |       17  |       32  |       49  | 
  -------------|-----------|-----------|-----------|
  Column Total |       91  |      109  |      200  | 
  -------------|-----------|-----------|-----------|
 
```

[TOC](#toc)

## Visualizing GLM <a name="vis_glm"></a>

```r
data <- read.csv("https://raw.githubusercontent.com/DrUzair/MLSD/master/Regression/binary_classification.csv")
sm <- glm(formula = Passed ~ Studied, data, family = 'binomial')
xstud <- seq(0, 10, 0.01)
ystud <- predict(sm, list(Studied = x_studies),type="response")
plot(d$Studied, d$Passed, pch = 16, xlab = "WEIGHT (g)", ylab = "VS")
lines(xstud, ystud)
```
![GLM](./glm_visual.png "Visualizing GLM") 

Notice how predict function is converting logodds into probability
```r
log_odds <- sm$coefficients['(Intercept)'] + sm$coefficients['Studied']*x_studies
p <- exp(log_odds)/(1+exp(log_odds))
par(mfrow=c(1,2))
plot(xstud, log_odds, xlab="Studied", ylab="Passed logodds", main='logodds\n(y=(w^{T}X))')
plot(data$Studied, data$Passed, xlab="Studied", ylab="Passed probability", main='probability\n(exp((wTX))/(1+exp(wTX))')
lines(xstud, p)
```
![GLM](./passed_studied_glm.png "Logodds and Probability")
The sign of the coefficient gives direction to sigmoid curve.
```r
wm <- glm(formula = vs ~ wt, family = binomial, data = mtcars)
xweight <- seq(0, 6, 0.01)
yweight <- predict(wm, list(wt = xweight),type="response")
log_odds <- wm$coefficients['(Intercept)'] + wm$coefficients['wt']*xweight
p <- exp(log_odds)/(1+exp(log_odds))
par(mfrow=c(1,2))
plot(xweight, log_odds, xlab="Weight", ylab="VS logodds", main='logodds\n(y=(w^{T}X))')
plot(mtcars$wt, mtcars$vs, xlab="Weight", ylab="VS probability", main='probability\n(exp(mx+b)/(1+exp((w^{T}X)))')
lines(xweight, p)
```
![GLM](./wt_vs_glm.png "Logodds and Probability")

[TOC](#toc)

# Effect of normalization <a name="normalization_effect"></a>
## Without normalization
```r
> data <- read.csv("https://raw.githubusercontent.com/DrUzair/MLSD/master/Regression/binary_classification.csv")
> glm(Passed~., data, family = 'binomial')

Call:  glm(formula = Passed ~ ., family = "binomial", data = data)

Coefficients:
(Intercept)      Studied        Slept  
    -16.160        1.745        1.487  

Degrees of Freedom: 99 Total (i.e. Null);  97 Residual
Null Deviance:	    137.6 
Residual Deviance: 43.4 	AIC: 49.4
```
## With normalization
```r
> data$Studied <- (data$Studied - mean(data$Studied))/sd(data$Studied)
> data$Slept <- (data$Slept - mean(data$Slept))/sd(data$Slept)
> glm(Passed~., data, family = 'binomial')

Call:  glm(formula = Passed ~ ., family = "binomial", data = data)

Coefficients:
(Intercept)      Studied        Slept  
      0.689        5.284        4.640  

Degrees of Freedom: 99 Total (i.e. Null);  97 Residual
Null Deviance:	    137.6 
Residual Deviance: 43.4 	AIC: 49.4
```
[TOC](#toc)

# Model Output Interpretation <a name="model_output"></a>

A typical output:
```r
glm(formula = hon ~ female + math, family = "binomial", data = hon_data)

Deviance Residuals: 
    Min       1Q   Median       3Q      Max  
-1.8494  -0.6506  -0.3471  -0.1361   2.5105  

Coefficients:
             Estimate Std. Error z value Pr(>|z|)    
(Intercept) -10.80595    1.61654  -6.685 2.32e-11 ***
female        0.96531    0.41599   2.321   0.0203 *  
math          0.16422    0.02665   6.161 7.21e-10 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

(Dispersion parameter for binomial family taken to be 1)

    Null deviance: 222.71  on 199  degrees of freedom
Residual deviance: 161.35  on 197  degrees of freedom
AIC: 167.35

Number of Fisher Scoring iterations: 5
```
## Deviance: 
Measure of Badness 0f Fit --> the lower the better

* Null Deviance:  How good/bad are the estimates of response variable using ONLY INTERCEPT or (grand mean)

* Residual Deciance: How better/worse are the estimates of response variable using other INDEPENDENT vars in addition to the INTERCEPT or (grand mean)

* DoF Null/Residual Deviance


## AIC: [Akaike Information Criterion]
* related concept ~ Adjusted R-squared: Penalize including inrrelevant variables.
   * Parsimony: unvderfitting (high bias) vs. overfitting (generalizability)
   * bias <--> variance of a model
* AIC = 2K - log(Likelihood( params | data) )2 
  * [Burnham & Anderson 2002]K: dof (estimable params)
* AICc = AIC + 2K(K+1)/(n-k-1) 
  * small sample correction [Hurvich&Tsai 1989]
* Akaike (1973) : what and how regressors influence dependent var

* The best model has the LOWEST AIC

  * Hosmer-Lemeshow Goodness of Fit: (another piece of measurement of goodness)
   - Measures difference between the model and the samples
   - library(ResourceSelection)
   - hoslem.test(mtcars$vs, fitted(model))
   - lower p-value(than alpha) indicates significant difference between obs and model output
 
 ##  Fisher Scoring Algorithm (maximum likelihood estimation)
 How many iterations it took to converge. 

[TOC](#toc)

