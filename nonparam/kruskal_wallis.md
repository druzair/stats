# Use of Ranks in One-Criterion Variance Analysis
William H. Kruskal and W. Allen Wallis: Use of Ranks in One-Criterion Variance Analysis, Journal of the American Statistical Association, Vol. 47, No. 260 (Dec., 1952), pp. 583-621
```math
H=\frac{12}{n(n+1)}\sum\frac{T_j^2}{n_j}-3(n+1) 
```

 
 
 Where:

 n = sum of samples in all groups \\

 T_j = sum of ranks in the jth grpup \\

 n_j = size of the jth group \\


## Explaination
The Kruskal Wallis test is a non parametric test, which means that the test doesn't assume your data comes from a particular distribution. 
The test is the non parametric alternative to the One Way ANOVA and is used when the assumptions for ANOVA aren't met (like the assumption of normality). 
It is sometimes called the one-way ANOVA on ranks, as the ranks of the data values are used in the test rather than the actual data points.
The test determines whether the medians of two or more groups are different. 
The hypotheses for the test are:
**H0: population medians are equal.**
**H1: population medians are not equal.**

# Sample question: A shoe company wants to know if three groups of workers have different salaries:
Women: 23K, 41K, 54K, 66K, 78K\
Men: 45K, 55K, 60K, 70K, 72K\
Minorities: 18K, 30K, 34K, 40K, 44K\

| id | salary | group    | rank |
|----|--------|----------|------|
| 1  | 45     | men      | 8    |
| 2  | 55     | men      | 10   |
| 3  | 60     | men      | 11   |
| 4  | 70     | men      | 13   |
| 5  | 72     | men      | 14   |
| 6  | 23     | women    | 2    |
| 7  | 41     | women    | 6    |
| 8  | 54     | women    | 9    |
| 9  | 66     | women    | 12   |
| 10 | 90     | women    | 15   |
| 11 | 20     | minority | 1    |
| 12 | 30     | minority | 3    |
| 13 | 34     | minority | 4    |
| 14 | 40     | minority | 5    |
| 15 | 44     | minority | 7    |

### Add up the different ranks for each group/sample.
Women: 23K, 41K, 54K, 66K, 90K = 2 + 6 + 9 + 12 + 15 = 44\
Men: 45K, 55K, 60K, 70K, 72K = 8 + 10 + 11 + 13 + 14 = 56\
Minorities: 20K, 30K, 34K, 40K, 44K = 1 + 3 + 4 + 5 + 7 = 20
 
###: Calculate the test statistic:
 h-test
 ```math
H=\frac{12}{n(n+1)}\sum\frac{T_j^2}{n_j}-3(n+1) \\

 H=[(\frac{12}{15(15+1)}) [ \frac{44^2}{5} + \frac{56^2}{5} + \frac{20^2}{5}] ]-3(15+1)
 ```
 
 **H* = 6.72
 
### Find the critical chi-square value. 
With c-1 degrees of freedom. For 3 - 1 degrees of freedom and an alpha level of .05, the critical chi square value is 0.03ish.
 
### Compare the H value from Step 4 to the critical chi-square value from Step 5.

If the critical chi-square value is less than the test statistic, reject the null hypothesis that the medians are equal.
 
The chi-square value is not less than the test statistic, so there is not enough evidence to suggest that the means are unequal.

```{r}
wages = data.frame('salary'=c(45, 55, 60, 70, 72, 23, 41, 54, 66, 90, 20, 30, 34, 40, 44), 'group'=c(rep('men', 5), rep('women', 5), rep('minority',5)))
boxplot(salary~group, data=wages)
kruskal.test(salary~group, data=wages) 
```
[Boxplot](https://gitlab.com/uzairg/stats/blob/master/nonparam/kw_exp.png)
