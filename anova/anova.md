# Understanding ANOVA


| Source     	| df  	| Sum of Squares 	| Mean Sum of Squares 	| F       	|
|------------	|-----	|----------------	|---------------------	|---------	|
| Treatments 	| k-1 	| SST            	| MST=SST/(k-1)       	| MST/MSE 	|
| Error      	| n-k 	| SSE            	| MSE=SSE/(n-k)       	|         	|
| Total      	| n-1 	| Total SS       	|                     	|         	|

ANOVA is a ratio of two number a.k.a F-ratio.
- Between-class variance (Sum of Squares for Treatments: SST)
    - Variation between k sample means  
```math
SST=\sum_{i=1}^{k} n_i(\bar{x_i} - \bar{x})^2 
```
- Within-class variance (Sum of Squares for Errors: SSE)
    - pooled variation within k samples
```math
SSE=\sum_{i=1}^{k} (n_i-1)s_i^2 
```
- The F-ratio follows [F distribution](https://www.stat.purdue.edu/~jtroisi/STAT350Spring2015/tables/FTable.pdf)

- The Total Sum of Squares is composed of SST and SSE

```math
TotalSS = SST + SSE
```
![ANOVA Illustration 1](./ANOVA_illustration_1.png)
![ANOVA Illustration 2](./ANOVA_illustration_2.png)
## Assumptions
- Normality: All groups/treatments are normally distributed
- Homogenous Variances: All groups/treatments have homogenous variance
    - For Heterogenous variances, oneway.test with var.equal=False

## Applications:
- Comparison of groups for atleast one being outstanding, H0: all groups are coming from same distribution
- Comparison of contributing of factors in a model (lm), H0: all factor are contributing equally to the model
- Comparison of multiple models, H0: all model produce same outputs 



## ANOVA in R
- anova: Compute analysis of variance (or deviance) tables for one or more fitted model objects.
- aov: Fit an analysis of variance model by a call to lm.
- oneway.test: Test whether two or more samples from normal distributions have the same means. The variances are not necessarily assumed to be equal.

### SSE: Within Class Variance
```math
SSE=\sum_{i=1}^{k} (n_i-1)s_i^2 \\
df=(n_1-1)+(n_2-1)+...+(n_k-1)=n-k
```
code
```R
within_class_var <- function(samples){

  wthn_sse_ = 0

  for(x in samples){
    wthn_sse_ <- wthn_sse_ + sum((x - mean(x))^2)
  }
  n = length(unlist(samples))
  k = length(samples)
  wthn_mse_ <- wthn_sse_ / (n-k)

  return (
          list(wthn_sse=c(wthn_sse_), 
               wthn_mse=c(wthn_mse_) 
               )
          )
}
```

### SST: Between Class Variance
```math
SST=\sum_{i=1}^{k} n_i(\bar{x_i} - \bar{x})^2 \\
df=n-1
```
code
```R
between_class_var <- function(samples){
  btwn_sst_ = 0
  mean_global = mean(unlist(samples))
  for(s in samples){
    btwn_sst_ = btwn_sst_ + sum((length(s))*(mean(s) - mean_global)^2)
  }
  btwn_mst_ <- btwn_sst_ / (length(samples)-1)
  return (
          list(btwn_sst=c(btwn_sst_),
               btwn_mst=c(btwn_mst_)
               )
          )
}
```

#### Total Sum of Squared distances
- TotalSS Sum of squared distances of each point from the mean of all data points.

```math
TotalSS = SSE + SST
```
Verify
```R
Total_SS <- sum((x - mean(x))^2)

```


### Calculating The F-Ratio & p-Value
- F=sst/sse
- [F distribution](https://www.stat.purdue.edu/~jtroisi/STAT350Spring2015/tables/FTable.pdf)
```R
anova_fn <- function(samples){
  wthn_class <- within_class_var(samples)
  btwn_class <- between_class_var(samples)
  F_ratio <- btwn_class$btwn_mst / wthn_class$wthn_mse
  k = length(samples)
  n = length(unlist(samples))
  p_val <- pf(F_ratio, df1=k-1, df2=n-k, lower.tail = FALSE)
  return(list(Wthn_MSE=wthn_class$wthn_mse,
              Btw_MST=btwn_class$btwn_mst,
              F_ratio=c(F_ratio), 
              p_Value=c(p_val)
              )
         )
}
```


## Simulate Data
Input 3 sample means and stds

```R
gen_data <- function(seed, N, means, stds) {
  set.seed(seed)
  lst = list()
  for (i in 1:length(means)){
    lst[[i]] <- rnorm(N, mean = means[i], sd = stds[i])
  }
  labels <- rep(1:length(means), rep(N, length(means)))
  labels <- as.factor(labels)
  abc_df <- data.frame(samples=unlist(lst), treatments=labels)
  return (list(df=abc_df, lsts=lst))
}
```
Plot the simulated data
```R
boxplot(samples~treatments, gen_data(seed=0, N=10, means=c(0,0,0), stds=c(1,1,1)))
```
## Simulate
```R
sim_results <- data.frame('seed'=0,'Btw_MST'=0, 'Wthn_MSE'=0,'F_ratio'=0, 'p_Value'=0)
N = 50
for(i in 1:100) {
  data <- gen_data(seed=i, N, means=c(5,5,5), stds=c(1, 1, 1))
  rslt <- anova_fn(data$lsts)
  df <- data.frame('seed'=i, 
                   'Btw_MST'=rslt$Btw_MST, 
                   'Wthn_MSE'=rslt$Wthn_MSE,
                   'F_ratio'=rslt$F_ratio, 
                   'p_Value'=rslt$p_Value)
  sim_results <- rbind(sim_results, df)
}
```
Plot and view the p-values leading the false-rejection of h0.
```R
attach(sim_results)
plot(F_ratio, p_Value)
hist(p_Value, breaks=seq(from =0, to = 1, by = .05))
sim_results[order(F_ratio, p_Value),]
```
# Note:
- for p_Value near above 0.05 to 1, we are sure H0 cannot be rejected
- for p_Value less than 0.05, we cannot accept H0
- Such small p_Value occur when F is larger than 3
- F larger than 3 means Between-Class-Variance(Btw_MST) is three times larger than Within-class-Variance(Wthn_MSE)
```R
anova_sim(seed=73, N=50, means=c(5,5,5), stds=c(1, 1, 1))
```
