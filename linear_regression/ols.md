# Regression / Ordinary Least Squares

[Inteactive Tutorial](https://spimelab.com/learn/ols)
## Simple Linear Regreassion

To Fall back (return) to the global mean

### Best-fit Linear model of the data: 
No other line can produce less error than the Best-fit line

## 3 Assumptions
 
# The Francis Galton's Dataset
__Extremely Tall__ parents produce __tall kids__, but not as much ... __Substantially shorter__

__Extremely Short__ parents produce __short kids__, but not as much ... __substantially taller__

Data set

Install/Load the UsingR package
```R
install.packages('UsingR')
require('UsingR')
```

### Plot Galton's Dataset
```R
data(galton)
head(galton)
dim(galton)
plot(galton$parent, Galton$child,
     main='Taller Parents --> Tall Kids ... but not as much, a little shorter.',
     xlab='Parent`s Height',
     ylab='Child`s height')
plot(jitter(galton$parent), jitter(Galton$child), 
     main='Taller Parents --> Tall Kids ... but not as much, a little shorter.',
     xlab='Parent`s Height',
     ylab='Child`s height')
```

```R
mChild <- mean(galton$child)
mParent <- mean(galton$parent)

points(mParent, mChild, type = "p", col='red')
abline(lm(galton$child~galton$parent))

fit <- lm(child~parent, data=galton)
summary(fit) 

```
![Galton's Dataset](ols/Galton_Dataset.png)

# Global mean

Any line, claiming to be **best fit** should pass through the **global mean**

All other points in the data will **Regress** to that line

```R
mChild <- mean(galton$child)
mParent <- mean(galton$parent)
points(mParent, mChild, type = "p", col='red')
```
![Global Mean of Galton's Dataset](ols/Galton_Dataset_Mean.png)
# Best-fit line

```R
best_fit_line = lm(galton$child~galton$parent)
abline(best_fit_line)
```
![Galton's Dataset](ols/Galton_Dataset_lm.png)

# Notes
```R
summary(lm(child~parent, data=galton))

Call:
lm(formula = child ~ parent, data = galton)

Residuals:
    Min      1Q  Median      3Q     Max 
-7.8050 -1.3661  0.0487  1.6339  5.9264 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept) 23.94153    2.81088   8.517   <2e-16 ***
parent       0.64629    0.04114  15.711   <2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 2.239 on 926 degrees of freedom
Multiple R-squared:  0.2105,	Adjusted R-squared:  0.2096 
F-statistic: 246.8 on 1 and 926 DF,  p-value: < 2.2e-16
```

### Residuals:
Model output - Actual value:

Heteroskedasticity

### Coefficients
```math
\hat{y}=mx+b
```
**slope = 0.64**
One unit change in the x/predictor/independent variable (parent's height) will cause +0.64 times change in y/the dependent variable. 
```math
m = \frac{\sum_{i=1}^{n}(x_i-\bar{x})(y_i-\bar{y})}{\sum_{i=1}^{n}(y_i-\bar{y})^2}
```
**Intercept = 23.94**
If the information about independent variable is not available, the average height will be 23.94??
```math
b = \bar{y} - m\bar{x}
```

#### t-Test
Null: slope is zero (no relationship between x and y)

```math
t=\frac{b-\beta}{\sqrt{MSE/S_{xx}}}
df = n-2

```

### R-squared 
Explained variation (RegressionSS) / Total variation (0~100%) 
```math
Total_{SS} = \sum(y_i - \bar{y})^2 \\ \\
Regression_{SS} = \sum(\hat{y} - \bar{y})^2 \\ \\
R^2 = \frac{Regression_{ss}}{Total_{SS}}
```
100% implies that the model explains all the variability of the response data around its mean.
low R-square and a good model case

Adjusted R-squared: (adjusted for different number of predictors)
more predictor may increase R-square but Adjustment will penalize adding useless independent variables

### F-statistics
- Catergorical Independant Variable
The F-statistic is testing the hypothesis that all levels/groups in the factor variable have zero mean. The first level/group/treatment is considered as the reference/baseline group.

- Continuous Independant Variable
The larger the more unlikely that coefficients have no effect. 
Compared to random guess, how better is regression model compared to others

- Same F-test is carried out for both of the following statements
```R
anova(lm(mpg~wt, data=mtcars))
summary(lm(mpg~wt, data=mtcars))
```
# The Intercept = Mean of Dependent Variable
***If*** the independent variables are standardized, the intercept (for only linear regression) is the same value as the mean of dependent variable.
```R
model <- lm(mpg ~ scale(wt), data=mtcars)
model$coefficients[1]
(Intercept) 
   20.09062 
mean(mtcars$mpg)
[1] 20.09062
```

# Correlation ~ Regression
***if*** both dependent and independent variables are scaled, then intercept (mean of dependent) is zero and slope/coefficient is equal to correlation.
```R
galton$childX <- scale(galton$child)
galton$parentX <- scale(galton$parent)
head(galton)
plot(galton$parentX, galton$childX)
fitx <- lm(galton$childX~galton$parentX)
fitx$coefficients[2] 
cor(galton$childX, galton$parentX)
abline(fitx, col='pink')
```
# Diagonostic Tools
## Residual Plots

# Confidence Interval vs Prediction Interval
Sampling process repeated large number (inifitiy) of time, (1-alpha)% of the outcomes will be in the range of 

### The model
```math
m = parent, b = intercept \\
\hat{y} = mx + b \\ \\
```

```math
E(\hat{m}) = m   \quad , \quad E(\hat{b}) = b \\ 
\\ \sigma^{2} \cong \hat{\sigma}^2 = MSE = \frac{\sum_{i=1}^{N}(y-\hat{y})^{2} } {n-2} \quad , \quad S_{xx}=\sum_{i=1}^{N}(x_i-\bar{x})^2 \\ 

\\ Var(\hat{m}) = \frac{ \sigma ^{2} } {{S_{xx}} } \quad , \quad Var(\hat{b}) =  \sigma^{2}\left [ \frac{1}{n} + \frac{\bar{x}^2}{S_{xx}} \right ] \\ 

\\ se(\hat{m}) = \sqrt{ Var(\hat{m})} \quad , \quad se(\hat{b}) = \sqrt{Var(\hat{b})}
```
### Null Hypothesis
```math
H_{0}: \hat{m} = 0 \quad , \quad H_{0}: \hat{b} = 0 \\
H_{a}: \hat{m} \neq 0 \quad , \quad H_{a}: \hat{b} \neq 0
```
### T-statistic
```math
T_{m} = \frac{\hat{m}-0}{se(\hat{m})} \quad , \quad T_{b} = \frac{\hat{b}-0}{se(\hat(b))}
```

### CI
Estimating the average value of Y (Child's height)
The model estimate computed based on given samples. Should the samples change, model is bound to change albeit slightly. All model variations are attempting to find the best fit **line of means**
The variance of model variations due to samples is 
```math
\hat{m} \pm t^*_{\frac{\alpha}{2}, n-2} \times se(\hat{m}) \quad , \quad \hat{b} \pm t^*_{\frac{\alpha}{2}, n-2} \times se(\hat{b})
```

### PI
Estimating a particular value of Y (Child's height)
Since it containis two error components. 
```math
\hat{y} \pm t_{\frac{\alpha}{2}, n-2} \times se(\hat{y}) \quad , \quad \hat{b} \pm t_{\frac{\alpha}{2}, n-2} \times \sqrt{\sigma^{2}\left [ 1 + \frac{1}{n} + \frac{\bar{x}^2}{S_{xx}} \right ]}
```

## Calculating Regression coefficients 

### Single Indepenedent variable
```math
\beta_1=\frac{cov(x, y)}{var(x)}
```
```r
simple_ols <- function(X,y){
  #slope <- sum((X - mean(X)) * (y - mean(y))) / sum((X-mean(X))^2)
  slope <- cov(X,y)/var(X)
  intercept <- mean(y) - slope*mean(X)
  return(c(intercept, slope))
}
```

### Multiple Independent variables
$`\beta_1=\frac{cov(x1, y)}{var(x1)}`$ and $`\beta_2=\frac{cov(x2, y)}{var(x2)}`$ may not hold because of possible collinearity $`cov(x1, x2)`$

Removing the collinearity is achieved by means of partial-variance. Instead of using actual values, the residuals of regression model are used.

#### Matrix Algebra
```r
reg_matrix_algebra <- function(X, y){
  X <- as.matrix(X)
  X <- cbind(rep(1, nrow(X)), X)
  y <- as.matrix(y)
  inv_Xt_x <- inv(t(X) %*% X) 
  Xt_y <- t(X) %*% y
  thetas <- inv_Xt_x %*% Xt_y
  return(thetas)
}
```

#### Inverse of Covariance Method
```r
reg_inv_cov <- function(X, y){
  yX <- cbind(y=y, X)
  C <- cov(yX)
  # Inverse of covariance matrix of yX (a.k.a Anti-Image Covariance Matrix)
  # inv_C diagonal elements are = 1/var(residuals of lm(yX[,j]~.))
  # it's partial-variance: diagonal elements of inv_C
  inv_C <- inv(C)
  coef_list <- c()
  for ( i in 2:ncol(yX)){
    coef_list <- c(coef_list, - inv_C[1,i] * (1/inv_C[1,1]) )
  }
  intercept <- 0
  for (i in 1:ncol(X)){
    intercept <- intercept + coef_list[i] * mean(X[,i])
  }
  intercept <- mean(y) - intercept 
  coef_list <- c(intercept, coef_list)
  return(coef_list)
}
```

concept
```r
r <- lm(yX[,1] ~ yX[,-1])$residuals
r1 <- lm(yX[,2] ~ yX[,-2])$residuals
r2 <- lm(yX[,3] ~ yX[,-3])$residuals
# inv_C diagonal elements are = 1/var(lm(yX[,j]~.)$residuals) a.k.a partial-variance
inv_C_11 <- 1/var(r)
inv_C_22 <- 1/var(r1)
inv_C_33 <- 1/var(r2)
# inv_C non-diagonal elements are = -cor(lm(yX[,i]~.)$residuals, lm(yX[,j]~.)$residuals)/(sd(lm(yX[,i]~.)$residuals)*sd(lm(yX[,j]~.)$residuals))
inv_C_12 <- cor(r, r1)/(sd(r)*sd(r1))
inv_C_13 <- cor(r, r2)/(sd(r)*sd(r2))
inv_C_23 <- cor(r1, r2)/(sd(r1)*sd(r2))
```

#### Covariance Matrix method
```r
reg_cov <- function(X, y){
  r <- lm(y~X)$residuals
  coef_list <- c()
  for (i in 1:ncol(X)){
    ri <- lm(X[,i] ~ y + X[,-i])$residuals
    coef_list <- c(coef_list, -cov(r, ri)/var(ri))
  }
  intercept <- 0
  for (i in 1:ncol(X)){
    intercept <- intercept + coef_list[i] * mean(X[,i])
  }
  intercept <- mean(y) - intercept 
  coef_list <- c(intercept, coef_list)
  return(coef_list)
}
```