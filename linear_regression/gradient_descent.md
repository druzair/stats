```math
y_i=w_0 + w_1x
\\
E(w_0, w_1) =  \sum_{i=1}^{N}(\hat{y}_i - y_i)^2
\\
\frac{\partial }{\partial w_0} J = \frac{\partial }{\partial w_0} \frac{1}{2}\sum_{i=1}^{N}(w_0 + w_1x_i - y_i)^2
\\
\frac{\partial }{\partial w_0} J = \frac{1}{2} \frac{\partial }{\partial w_0} \sum_{i=1}^{N}(w_0 + w_1x_i - y_i)^2
\\
\frac{\partial }{\partial w_0} J = \frac{1}{2} \sum_{i=1}^{N}2(w_0 + w_1x_i - y_i) \frac{\partial }{\partial w_0}(w_0 + w_1x_i - y_i)
\\
\frac{\partial }{\partial w_0} J = \sum_{i=1}^{N}(w_0 + w_1x_i - y_i) (1)
\\
```
